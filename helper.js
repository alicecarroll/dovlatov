function getSlide(caro, slide) {
  let image = $(
    `.t-slds__items-wrapper:nth(${caro}) > div[data-slide-index=${
      slide + 1
    }] meta[itemprop="image"]`
  )[0].content;
  let captions = $(
    `.t-slds__caption__container:nth(${caro}) > div:nth-child(${
      slide + 1
    }) > div > div`
  )
    .toArray()
    .map((i) => i.innerText);
  return {
    src: `${window.location.toString().replace(/\/$/, "")}/${image}`,
    title: captions[0],
    desc: captions[1],
  };
}

function getCaro(index) {
  let arr = [];
  let i = 0;
  while (true) {
    let slide = getSlide(index, i);
    if (slide.title === undefined) {
      break;
    }
    arr.push(slide);
    ++i;
  }
  return arr;
}
